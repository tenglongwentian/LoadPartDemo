﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LoadPartDemo.Startup))]
namespace LoadPartDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
